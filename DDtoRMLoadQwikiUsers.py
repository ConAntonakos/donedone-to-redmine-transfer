from DDtoRMPostQwikiUsers import *
import requests, json, simplejson


def loadQwikiUsers(url, nameArray):
	print('\nCreating Qwiki users and pushing to Redmine. Please wait...')
	for qwikiTeamMember in nameArray:
		# In a separate module
		postQwikiUsersHelper(url, qwikiTeamMember['name'], qwikiTeamMember['mail'])
	print('\nAll done!')


def prepareUserInfoHashFromRedmine(url):
	userInfoArrayofHashes = []
	qwikiUserHash = {}
	username = 'admin'
	password = 'admin'

	r = requests.get(url, auth=(username, password))
	json = r.json()

	for eachUser in json['users']:
		qwikiUserHash = {
							'id': eachUser['id'],
							'login': eachUser['login'],
							'firstname': eachUser['firstname'],
							'lastname': eachUser['lastname'],
							'fullname': eachUser['firstname'] + ' ' + eachUser['lastname'],
							'mail': eachUser['mail'],
							'created_on': eachUser['created_on'],
						}
		userInfoArrayofHashes.append(qwikiUserHash)

	return userInfoArrayofHashes