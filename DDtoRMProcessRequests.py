import requests, json, time, sys

def openURLWithRequestsModule(url, username, password, payload, headers, requestType):
	if requestType == 'PUT':
		parameters_json = json.dumps(payload)
		r = requests.put(url, auth=(username, password), data=parameters_json, headers=headers)
		sys.stdout.write('.')
		sys.stdout.flush()

	elif requestType == 'POST':
		parameters_json = json.dumps(payload)
		r = requests.post(url, auth=(username, password), data=parameters_json, headers=headers)
		sys.stdout.write('.')
		sys.stdout.flush()