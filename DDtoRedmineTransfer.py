from DoneDone import IssueTracker
from DDtoRMMethodDefs import *
from DDtoRMLoadQwikiUsers import *
import json, requests, pprint, time

# DoneDone authentication
username = 'conanton'
password = 'qwiki4life'

# DoneDone URLs
baseProjectURL = 'https://qwiki.mydonedone.com/IssueTracker/API/IssuesInProject/'
baseIssueURL = 'https://qwiki.mydonedone.com/IssueTracker/API/Issue/'
projectID = '11736'

projectURL = baseProjectURL + projectID
baseIssueURLWithProjectID = baseIssueURL + projectID

# Redmine API
key = raw_input("Please enter your Redine admin API: ")
apiKey = str(key)

# Redmine URLs
domainURL = 'http://localhost:3000/'
issuesJsonExtension = 'issues.json'
usersJsonExtension = 'users.json'
apiKeyExtention = '?key=' + apiKey

redmineIssuesURL = domainURL + issuesJsonExtension + apiKeyExtention
redmineUsersURL = domainURL + usersJsonExtension + apiKeyExtention

# Begin process: open DoneDone URL (projectURL) via Requests module
r = requests.get(projectURL, auth=(username, password))
json = r.json()

# Prepare a hash of arrays that will contain all the values extracted from DoneDone in order to transfer into Redmine
allParametersDict = {}
allParametersDict['TitleArray'] = []
allParametersDict['CreatorName'] = []
allParametersDict['ResolverName'] = []
allParametersDict['CreateDateArray'] = []
allParametersDict['OrderNumberArray'] = []
allParametersDict['IssueURLArray'] = []
allParametersDict['DescriptionArray'] = []
allParametersDict['QwikiUserAndCommentArrayofHashes'] = []
qwikiTeamNames = [{'name': 'Danielle Abraham', 'mail':'danielle@qwiki.com'}, {'name':'Constantine Antonakos', 'mail':'constantine@qwiki.com'},\
					{'name':'Owen Bossola', 'mail':'o@qwiki.com'}, {'name':'Pete Callaway', 'mail':'pete@qwiki.com'},\
					{'name':"Tommy Chheng", 'mail':'t@qwiki.com'}, {'name':"Greg Pape", 'mail':'gp@qwiki.com'}, \
					{'name':"Phil Pape", 'mail':'phil@qwiki.com'}, {'name':"Tim Holman", 'mail':'tim@qwiki.com'}, \
					{'name':"Doug Imbruce", 'mail':'d@qwiki.com'}, {'name':"Greg Ingelmo", 'mail':'gi@qwiki.com'}, \
					{'name':"Rasmus Knutsson", 'mail':'r@qwiki.com'}, {'name':"Sheena Lee", 'mail':'sheena@qwiki.com'},\
					{'name':'unnassigned user', 'mail':'ingelmo@gmail.com'}]

# key = raw_input("Enter key to search: ")
# while key:
# 	allParametersDict[key.replace(" ", "") + "Array"] = []
# 	key = raw_input("Enter key to search: ")
# filename = raw_input("Enter file name: ") + '.txt'
# file = open(filename, 'w')

print('####################################################################')
print('###########################     Qwiki     ##########################')
print('#############       DoneDone to Redmine Transfer       #############')
print('####################################################################')

# Extract information from DoneDone
print('\nExtracting titles, descriptions and order numbers from DoneDone...')
for eachObject in json:
	prepareTitleArray(allParametersDict['TitleArray'], eachObject, 'Title')
	prepareCreateDateArray(allParametersDict['CreateDateArray'], eachObject, 'CreateDate')
	prepareOrderNumberArray(allParametersDict['OrderNumberArray'], eachObject, 'OrderNumber')
time.sleep(1)
print('Done!\n')

# Every DoneDone issue has its own URL, and it is required to loop through them in order to extract very granular details
prepareIssueURLArray(allParametersDict['IssueURLArray'], allParametersDict['OrderNumberArray'], baseIssueURLWithProjectID)	
prepareDescriptionCreatorNameQwikiUserAndCommentArray(allParametersDict['DescriptionArray'], allParametersDict['CreatorName'], allParametersDict['ResolverName'], allParametersDict['QwikiUserAndCommentArrayofHashes'], allParametersDict['IssueURLArray'], username, password)

#Upload Qwiki users to Redmine server
loadQwikiUsers(redmineUsersURL, qwikiTeamNames)

# Prepare a hash of user information for each user
# by getting the info from Redmine server
userInfoHash = prepareUserInfoHashFromRedmine(redmineUsersURL)

resultsArray = combineArrays(\
							allParametersDict['TitleArray'],\
							allParametersDict['DescriptionArray'],\
							allParametersDict['CreateDateArray'],\
							allParametersDict['CreatorName'],\
							allParametersDict['ResolverName']
							)

loadIssuesIntoRedmine(redmineIssuesURL, resultsArray, userInfoHash)

loadCommentsIntoRedmine(domainURL, apiKeyExtention, allParametersDict['QwikiUserAndCommentArrayofHashes'], userInfoHash)


### Debugging ###

# print resultsArray
# print allParametersDict['OrderNumberArray']		
# print allParametersDict['IssueURLArray']
# print allParametersDict['DescriptionArray'][0:2]

# print resultsArray[0:2]
# payload = loadJsonObject(resultsArray)
# print payload