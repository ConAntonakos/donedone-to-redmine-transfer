from DDtoRMProcessRequests import *
import requests, json, time, sys

def prepareTitleArray(titleArray, dictObj, key):
	title = dictObj[key]
	titleArray.append(title)
	
def prepareCreateDateArray(createDateArray, dictObj, key):
	unixDate = dictObj[key][6:19]
	epoch = float(unixDate)/1000
	timeLocal = time.localtime(epoch)
	# epoch = time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.localtime(epochTime))
	year = str(timeLocal[0])
	month = str(timeLocal[1])
	day = str(timeLocal[2])
	hour = str(timeLocal[3] - 5)
	minute = str(timeLocal[4])
	second = str(timeLocal[5])
	if int(month) < 10:
		if int(day) < 10:
			formatted = "%s-0%s-0%s" % (year, month, day)
		else:
			formatted = "%s-0%s-%s" % (year, month, day)
	elif int(day) < 10:
		formatted = "%s-%s-0%s" % (year, month, day)
	else:
		formatted = "%s-%s-%s" % (year, month, day)
	createDateArray.append(formatted)

def prepareOrderNumberArray(orderNumberArray, dictObj, key):
	orderNumberArray.append(dictObj[key])	

def prepareIssueURLArray(issueURLArray, orderNumberArray, projectURL):
	print('\nExtracting the URLs for each issue...')
	for orderNum in orderNumberArray:
		issueURL = projectURL + '/' + str(orderNum)
		issueURLArray.append(issueURL)
	time.sleep(1)
	print('Done!\n')



def prepareDescriptionCreatorNameQwikiUserAndCommentArray(descriptionArray, creatorNameArray, resolverNameArray, qwikiUserAndCommentArray, issueURLArray, username, password):
	print('\nExtracting descriptions and comments. Please wait...')
	for issueURL in issueURLArray:
		r = requests.get(issueURL, auth=(username, password))
		json = r.json()
		descriptionArray.append(json['Description'])
		creatorNameArray.append(json['Creator']['Value'])
		resolverNameArray.append(json['Resolver']['Value'])
		arrayTemp = []
		for jsonObj in json['Histories']:
			qwikiUserAndCommentHash = {}
			comment = jsonObj['Description']
			splitActionToRevealQwikiUser = jsonObj['Action'].split()
			qwikiUser = splitActionToRevealQwikiUser[0] + ' ' + splitActionToRevealQwikiUser[1]
			qwikiUserAndCommentHash['name'] = qwikiUser
			qwikiUserAndCommentHash['comment'] = comment
			arrayTemp.append(qwikiUserAndCommentHash)
		qwikiUserAndCommentArray.append(arrayTemp)
		sys.stdout.write('.')
		sys.stdout.flush()
	print(' (100%)')
	print('Done! All extracted!\n')
	# print(qwikiUsernameAndCommentArray[0:2])

# #####
# Combining arrays of individual parameters into one array to easily manipulate and get data
# Title is in the zeroth index
# Description is in the first index
# CreateDate is in the second index
# CreatorName is in the third index
# ResolverName is in the fourth index
# #####
def combineArrays(titleArray, descriptionArray, createDateArray, creatorNameArray, resolverNameArray):
	resultsArray = []
	for i in range(0, len(titleArray)):
		new = []
		new.append(titleArray[i])
		resultsArray.append(new)
	for i in range(0, len(descriptionArray)):
		resultsArray[i].append(descriptionArray[i])
	for i in range(0, len(createDateArray)):
		resultsArray[i].append(createDateArray[i])
	for i in range(0, len(creatorNameArray)):
		resultsArray[i].append(creatorNameArray[i])
	for i in range(0, len(resolverNameArray)):
		resultsArray[i].append(resolverNameArray[i])
	return resultsArray

def loadIssuesIntoRedmine(url, resultsArray, userInfoHash):
	print('\nPushing issues to Redmine server. Please wait...')
	requestType = 'POST'
	username = None
	password = 'qwikinyc'
	author_qwikiID = None
	assigned_to_qwikiID = None
	headers = None
	for i in range(0, len(resultsArray)):
		for qwikiUser in userInfoHash:
			if resultsArray[i][3] == qwikiUser['fullname']:
				author_qwikiID = qwikiUser['id']
				username = qwikiUser['login']
				headers = {'content-type': 'application/json', 'X-Redmine-Switch-User': qwikiUser['login']}	
			if resultsArray[i][4] == qwikiUser['fullname']:
				assigned_to_qwikiID = qwikiUser['id']

		payload = {
					'issue': {
							'project_id': 1,
							'tracker_id': 1,
							'status_id': 1,
							'author_id': author_qwikiID,
							'assigned_to_id': assigned_to_qwikiID,
							'subject': resultsArray[i][0],
							'description': resultsArray[i][1],
							'start_date': resultsArray[i][2]
					}
				}
		# return payload

		openURLWithRequestsModule(url, username, password, payload, headers, requestType)
	print(' (100%)')
	print('Complete!\n')
# 		# with open('test.txt', 'a') as jsonFile:
# 		# 	jsonFile.write(parameters_json)


def loadCommentsIntoRedmine(url, apiKeyExtention, qwikiUserAndCommentArray, userInfoHash):
	print('\nPushing comments to Redmine server. Please wait...')
	requestType = 'PUT'
	username = 'admin'
	password = 'admin'
	# redmineIssueArray = []
	# commentsPayloadArray = []
	for i in range(0, len(qwikiUserAndCommentArray)):
		individualRedmineIssueURL = url + 'issues/%s.json' % (i+1)
		newRedmineIssueWithAPIKeyURL = individualRedmineIssueURL + apiKeyExtention
		# redmineIssueArray.append(redmineIssueUpdateURL)
		if len(qwikiUserAndCommentArray[i]) > 1:
			for qwikiUserAndComment in qwikiUserAndCommentArray[i]:
				for qwikiUser in userInfoHash:
					if qwikiUserAndComment['name'] == qwikiUser['fullname']:
						headers = {'content-type': 'application/json', 'X-Redmine-Switch-User': qwikiUser['login']}	
						payload = {
									'issue': {
										'notes': qwikiUserAndComment['comment']
									}
								}
						openURLWithRequestsModule(newRedmineIssueWithAPIKeyURL, username, password, payload, headers, requestType)
		else:
			for qwikiUser in userInfoHash:
				if qwikiUserAndCommentArray[i][0]['name'] == qwikiUser['fullname']:
					headers = {'content-type': 'application/json', 'X-Redmine-Switch-User': qwikiUser['login']}
					payload = {
								'issue': {
									'notes': qwikiUserAndCommentArray[i][0]['comment']
								}
							}

					openURLWithRequestsModule(newRedmineIssueWithAPIKeyURL, username, password, payload, headers, requestType)
	print(' (100%)')
	print('\nWHAT?! IT\'S DONE? MIND. BLOWN.\n')