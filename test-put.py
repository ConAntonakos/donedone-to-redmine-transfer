import json, requests

payload = {
			"issue": {
				'author_id': 2,
				'subject:': 'CHANGED FROM SCRIPT',
				'notes': 'Testing USER ID'
			}
		}

username = 'admin'
password = 'admin'
url = 'http://localhost:3000/issues/78.json'
parameters_json = json.dumps(payload)
headers = {'content-type': 'application/json'}
r = requests.put(url, auth=(username, password), data=parameters_json, headers=headers)